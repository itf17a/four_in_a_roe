class Game():
    def __init__(self):
        self.columnCount = 7
        self.rowCount = 6

        self.current_player = 0
        self.count_column = []
        self.field = []
        self.setup()

    def setup(self):
        self.field = [[0 for x in range(self.columnCount)] for x in range(self.rowCount)]
        self.count_column = [0 for x in range(self.columnCount)]
    
    def _printNewLine(self):
        """ Helper Method for printField, prints a new-line with corresponding headers,.. """
        line = "+"
        for i in range(0, self.columnCount):
            line += '-----+'
        print(line)

    def _getTranslation(self, content):

        if content == 0:
            return "   "
        elif content == 1:
            return " X "
        elif content == 2:
            return " O "

    def printField(self):
        """ Print the field human-readable in stdout """
        print("\nVIER GEWINNT\n")
        self._printNewLine()
        for row in reversed(self.field):
            print('| ', end='')
            for column in row:
                print(self._getTranslation(column) + ' | ', end='')
            print()
            self._printNewLine()
        
        print("Player" + self._getTranslation(self.current_player))
    
    def add_stone(self, col):
        c = int(col)

        row = self.count_column[c]
        column = self.columnCount

        if row < column:
            self.field[row][c] = self.current_player
            self.count_column[c] = row + 1

            if self.current_player == 1:
                self.current_player = 2
            else: self.current_player = 1

        else:
            print("fehler")

    def checkWin(self):

        # Check horizontally
        for col in range(self.columnCount-3):
            for row in range(self.rowCount):
                if (self.field[row][col] == self.field[row][col+1] == self.field[row][col+2] == self.field[row][col+3] != 0):
                    return self.field[row][col]

        # Check vertically
        for row in range(self.rowCount-3):
            for col in range(self.columnCount):
                if (self.field[row][col] == self.field[row+1][col] == self.field[row+2][col] == self.field[row+3][col] != 0):
                    return self.field[row][col]

        # Skip diagonal checks if column count is less than 4
        if (self.columnCount < 4):
            return False

		# Check up-diagonally
        for col in range(self.columnCount-3):
            for row in range(self.rowCount-3):
                if (self.field[row][col] == self.field[row+1][col+1] == self.field[row+2][col+2] == self.field[row+3][col+3] != 0):
                    return self.field[row][col]

        # Check down-diagonally
        for col in range(3, self.columnCount):
            for row in range(self.rowCount-3):
                if (self.field[row][col] == self.field[row+1][col-1] == self.field[row+2][col-2] == self.field[row+3][col-3] != 0):
                    return self.field[row][col]

        return False

if "__main__" == __name__:
    a=Game()
    a.current_player = 1

    while True:
        a.printField()
        eingabe = input()
        a.add_stone(eingabe)
        if a.checkWin() != False:
            a.printField()
            print("Gewonnen hat: " + str(a.current_player))
            break;
